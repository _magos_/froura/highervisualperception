#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, json, sys, io

# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types

# Instantiates a client
client = vision.ImageAnnotatorClient()

def recognize_plate_google(img):
    """Recognize plate of vehicle in img by google cloud service."""
    global client

    # Loads the image into memory
    with io.open(img, 'rb') as image_file:
        content = image_file.read()

    image = types.Image(content=content)

    # Performs label detection on the image file
    response = client.text_detection(image=image)
    texts = response.text_annotations
    result = []
    if len(texts) > 1:
        result.append(texts[0].description.replace('\n',''))

    return result


def recognize_plate_alpr(img):
    """Recognize plate of vehicle in img."""
    cmd = 'alpr -n 1 -j ' + img
    ret = json.loads(os.popen('alpr -n 1 -j ' + img).read())
    plate = ret['results']

    return plate

def recognize_plate(img):
    return recognize_plate_alpr(img)
    #return recognize_plate_google(img)

if __name__ == '__main__':
    recognize_plate(sys.argv[1])

