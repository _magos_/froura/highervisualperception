#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, json, sys

detector_dir = os.environ['HIGHERVISUALPERCEPTION_DETECTOR_DIR']

def detect_vehicle(img):
    """Detect vehicles in img."""
    rets = os.popen('sh highervisualperception/highervisualperception/python/vehicledetection_script.sh ' + detector_dir + ' ' + img).read()

    results = []
    for ret in rets.split('#'):
        if len(ret) > 0 and ret[0] == '_':
            ret = ret.replace('_', '').replace('\n', '').split(' ')
        else:
            ret = []
        results.append(ret)

    return results

if __name__ == '__main__':
    detect_vehicle(sys.argv[1])

