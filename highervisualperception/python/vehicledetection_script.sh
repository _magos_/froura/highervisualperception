#!/bin/sh

HIGHERVISUALPERCEPTION_DETECTOR_DIR=$1
IMAGE_DIR=$2
cd $HIGHERVISUALPERCEPTION_DETECTOR_DIR
 ./build/yolo_mobilenet_recognizer  highervisualperception/data/models/mobilenet_yolov3_lite_deploy.prototxt highervisualperception/data/models/mobilenet_yolov3_lite_deploy.caffemodel -file_type single_image -wait_time 1 -mean_value 0.5,0.5,0.5 -normalize_value 0.007843 -confidence_threshold 0.3 $IMAGE_DIR

