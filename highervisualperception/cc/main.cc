#include <caffe/caffe.hpp>
#include <chrono>
#include "ssd_detect.h"

using namespace std::chrono;
using namespace caffe;  // NOLINT(build/namespaces)

//#define custom_class
#ifdef custom_class
char* CLASSES[81] = {
    "__background__", "person", "bicycle", "car", "motorcycle",
    "airplane", "bus", "train", "truck", "boat",
    "traffic light", "fire hydrant", "stop sign", "parking meter",
    "bench", "bird", "cat",
    "dog", "horse", "sheep", "cow",
    "elephant", "bear", "zebra", "giraffe" ,
    "backpack", "umbrella", "handbag", "tie" ,
    "suitcase", "frisbee", "skis", "snowboard" ,
    "sports ball", "kite", "baseball bat", "baseball glove" ,
    "skateboard", "surfboard", "tennis racket", "bottle" ,
    "wine glass", "cup", "fork", "knife" ,
    "spoon", "bowl", "banana", "apple" ,
    "sandwich", "orange", "broccoli", "carrot" ,
    "hot dog", "pizza", "donut", "cake" ,
    "chair", "couch", "potted plant", "bed" ,
    "dining table", "toilet", "tv", "laptop" ,
    "mouse", "remote", "keyboard", "cell phone" ,
    "microwave", "oven", "toaster", "sink" ,
    "refrigerator", "book", "clock", "vase" ,
    "scissors", "teddy bear", "hair drier", "toothbrush" ,
};
#else
char* CLASSES[21] = {
    "__background__", "aeroplane", "bicycle", "bird", "boat",
    "bottle", "bus", "car", "cat", "chair",
    "cow", "diningtable", "dog", "horse",
    "motorbike", "person", "pottedplant",
    "sheep", "sofa", "train", "tvmonitor"
};
#endif

DEFINE_string(mean_file, "",
        "The mean file used to subtract from the input image.");
DEFINE_string(mean_value, "104,117,123",
        "If specified, can be one value or can be same as image channels"
        " - would subtract from the corresponding channel). Separated by ','."
        "Either mean_file or mean_value should be provided, not both.");
DEFINE_string(file_type, "image",
        "The file type in the list_file. Currently support image and video.");
DEFINE_string(out_file, "",
        "If provided, store the detection results in the out_file.");
DEFINE_double(confidence_threshold, 0.90,
        "Only store detections with score higher than the threshold.");
DEFINE_double(normalize_value, 1.0,
        "Normalize image to 0~1");
DEFINE_int32(wait_time, 1000,
        "cv imshow window waiting time ");

std::string process_image(cv::Mat img, Detector detector, float confidence_threshold)
{
    // Do detection.
    std::vector<vector<float> > detections = detector.Detect(img);

    // Prepare to return the result.
    string ret = "";
    for (int i = 0; i < detections.size(); ++i)
    {
        const vector<float>& d = detections[i];
        // Detection format: [image_id, label, score, xmin, ymin, xmax, ymax].
        CHECK_EQ(d.size(), 7);
        const float score = d[2];
        if (score >= confidence_threshold)
        {
            ret = ret + "_" + CLASSES[static_cast<int>(d[1])] + " ";

            ret = ret + std::to_string(score) + " ";
            ret = ret + std::to_string(static_cast<int>(d[3] * img.cols)) + " ";
            ret = ret + std::to_string(static_cast<int>(d[4] * img.rows)) + " ";
            ret = ret + std::to_string(static_cast<int>(d[5] * img.cols)) + " ";
            ret = ret + std::to_string(static_cast<int>(d[6] * img.rows));
            if (i != detections.size() -1)
            {
                ret = ret + "#";
            }
        }
    }

    return ret;
}

int main(int argc, char** argv)
{
    ::google::InitGoogleLogging(argv[0]);
    // Print output to stderr (while still logging)
    //FLAGS_alsologtostderr = 1;
    // Disable caffe logging module
    google::SetCommandLineOption("GLOG_minloglevel", "1");

    gflags::SetUsageMessage("Do detection using SSD mode.\n"
            "Usage:\n"
            "    ssd_detect [FLAGS] model_file weights_file list_file\n");
    gflags::ParseCommandLineFlags(&argc, &argv, true);

    if (argc < 3)
    {
        gflags::ShowUsageWithFlagsRestrict(argv[0], "examples/ssd/ssd_detect");
        return 1;
    }

    const string& model_file = argv[1];
    const string& weights_file = argv[2];
    const string& mean_file = FLAGS_mean_file;
    const string& mean_value = FLAGS_mean_value;
    const string& file_type = FLAGS_file_type;
    const string& out_file = FLAGS_out_file;
    const float& confidence_threshold = FLAGS_confidence_threshold;
    const float& normalize_value = FLAGS_normalize_value;
    const int& wait_time = FLAGS_wait_time;

    // Initialize the network.
    Detector detector(model_file, weights_file, mean_file, mean_value, confidence_threshold, normalize_value);

    // Process image one by one.
    //std::ifstream infile(argv[3]);
    const string &indir = "//data";
    std::string file;
    //out << file_type <<"demo";
    if (file_type == "image")
    {
        char buf[1000];
        sprintf(buf, "%s/*.jpg", "data//");
        cv::String path(buf); //select only jpg

        vector<cv::String> fn;
        vector<cv::Mat> data;
        cv::glob(path, fn, true); // recurse
        for (size_t k = 0; k < fn.size(); ++k)
        {
            //cv::Mat img = cv::imread("data//000166.jpg");
            cv::Mat img = cv::imread(fn[k]);
            if (img.empty()) continue; //only proceed if sucsessful
            // you probably want to do some preprocessing
            CHECK(!img.empty()) << "Unable to decode image " << file;
            Timer batch_timer;
            batch_timer.Start();
            std::vector<vector<float> > detections = detector.Detect(img);
            //LOG(INFO) << "Computing time: " << batch_timer.MilliSeconds() << " ms.";
            /* Print the detection results. */
            for (int i = 0; i < detections.size(); ++i)
            {
                const vector<float>& d = detections[i];
                // Detection format: [image_id, label, score, xmin, ymin, xmax, ymax].
                CHECK_EQ(d.size(), 7);
                const float score = d[2];
                if (score >= confidence_threshold)
                {
                    /*
                       out << file << " ";
                       out << static_cast<int>(d[1]) << " ";
                       out << score << " ";
                       out << static_cast<int>(d[3] * img.cols) << " ";
                       out << static_cast<int>(d[4] * img.rows) << " ";
                       out << static_cast<int>(d[5] * img.cols) << " ";
                       out << static_cast<int>(d[6] * img.rows) << std::endl;
                     */

                    cv::Point pt1, pt2;
                    pt1.x = (img.cols*d[3]);
                    pt1.y = (img.rows*d[4]);
                    pt2.x = (img.cols*d[5]);
                    pt2.y = (img.rows*d[6]);
                    cv::rectangle(img, pt1, pt2, cvScalar(0, 255, 0), 1, 8, 0);

                    char label[100];
                    sprintf(label, "%s,%f", CLASSES[static_cast<int>(d[1])], score);
                    std::cout << "Recognised: ";
                    std::cout << CLASSES[static_cast<int>(d[1])];
                    std::cout << "; " << score;
                    std::cout << "; [ " << img.cols*d[3] << ", " << img.rows*d[4]  << ", " << img.cols*d[5]  << ", " << img.rows*d[6] << " ]";
                    std::cout << std::endl;
                    int baseline;
                    cv::Size size = cv::getTextSize(label, cv::FONT_HERSHEY_SIMPLEX, 0.5, 0, &baseline);
                    cv::Point pt3;
                    pt3.x = pt1.x + size.width;
                    pt3.y = pt1.y - size.height;
                    cv::rectangle(img, pt1, pt3, cvScalar(0, 255, 0), -1);

                    cv::putText(img, label, pt1, cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 0));
                }
            }
            cv::imshow("show", img);
            sprintf(buf, "out//%05d.jpg", k);
            cv::imwrite(buf, img);
            cv::waitKey(wait_time);
            data.push_back(img);
        }
    }
    else if (file_type == "webcam")
    {
        for (size_t k = 0; k < 1; ++k)
        {
            cv::VideoCapture cap(0);

            if (!cap.isOpened())
            {
                //LOG(FATAL) << "Failed to open camera";
            }
            cv::Mat img;
            int frame_count = 0;
            while (true)
            {
                bool success = cap.read(img);
                if (!success)
                {
                    //LOG(INFO) << "Process " << frame_count << " frames from " << file;
                    break;
                }
                CHECK(!img.empty()) << "Error when read frame";
                std::vector<vector<float> > detections = detector.Detect(img);

                /* Print the detection results. */
                for (int i = 0; i < detections.size(); ++i)
                {
                    const vector<float>& d = detections[i];
                    // Detection format: [image_id, label, score, xmin, ymin, xmax, ymax].
                    CHECK_EQ(d.size(), 7);
                    const float score = d[2];
                    if (score >= confidence_threshold)
                    {
                        /*
                           out << file << "_";
                           out << std::setfill('0') << std::setw(6) << frame_count << " ";
                           out << static_cast<int>(d[1]) << " ";
                           out << score << " ";
                           out << static_cast<int>(d[3] * img.cols) << " ";
                           out << static_cast<int>(d[4] * img.rows) << " ";
                           out << static_cast<int>(d[5] * img.cols) << " ";
                           out << static_cast<int>(d[6] * img.rows) << std::endl;
                         */

                        cv::Point pt1, pt2;
                        pt1.x = (img.cols*d[3]);
                        pt1.y = (img.rows*d[4]);
                        pt2.x = (img.cols*d[5]);
                        pt2.y = (img.rows*d[6]);
                        int index = static_cast<int>(d[1]);
                        int green = 255 * ((index + 1) % 3);
                        int blue = 255 * (index % 3);
                        int red = 255 * ((index + 1) % 4);
                        cv::rectangle(img, pt1, pt2, cvScalar(red, green, blue), 1, 8, 0);

                        char label[100];
                        sprintf(label, "%s,%f", CLASSES[static_cast<int>(d[1])], score);
                        int baseline;
                        cv::Size size = cv::getTextSize(label, cv::FONT_HERSHEY_SIMPLEX, 0.5, 0, &baseline);
                        cv::Point pt3;
                        pt3.x = pt1.x + size.width;
                        pt3.y = pt1.y - size.height;

                        cv::rectangle(img, pt1, pt3, cvScalar(red, green, blue), -1);

                        cv::putText(img, label, pt1, cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 0));
                    }
                }
                cv::imshow("show", img);
                if (cv::waitKey(30) >= 0)
                {
                    break;
                }
                ++frame_count;
            }
            if (cap.isOpened())
            {
                cap.release();
            }
        }
    }
    else if (file_type == "single_image")
    {
        cv::Mat img = cv::imread(argv[3]);
        if (img.empty())
            return -1;

        high_resolution_clock::time_point t1 = high_resolution_clock::now();

        std::string result = process_image(img, detector, confidence_threshold);

        high_resolution_clock::time_point t2 = high_resolution_clock::now();
        //std::cout << "Process time: " << duration_cast<microseconds>( t2 - t1 ).count() << std::endl;

        std::cout << result << std::endl;
    }
    else 
    {
        char buf[1000];
        sprintf(buf, "%s/*.avi", "data//");
        cv::String path(buf); //select only jpg
        int count = 0;
        vector<cv::String> fn;
        vector<cv::Mat> data;
        cv::glob(path, fn, true); // recurse
        int max = 14500;
        for (size_t k = 0; k < fn.size(); ++k)
        {
            //out << fn[k] << std::endl;
            cv::VideoCapture cap(fn[k]);

            if (!cap.isOpened())
            {
                //LOG(FATAL) << "Failed to open video: " << file;
            }
            cv::Mat img;
            int frame_count = 0;
            while (true)
            {
                bool success = cap.read(img);
                if (!success)
                {
                    //LOG(INFO) << "Process " << frame_count << " frames from " << file;
                    break;
                }
                CHECK(!img.empty()) << "Error when read frame";
                std::vector<vector<float> > detections = detector.Detect(img);

                /* Print the detection results. */
                for (int i = 0; i < detections.size(); ++i)
                {
                    const vector<float>& d = detections[i];
                    // Detection format: [image_id, label, score, xmin, ymin, xmax, ymax].
                    CHECK_EQ(d.size(), 7);
                    const float score = d[2];
                    if (score >= confidence_threshold)
                    {
                        /*
                           out << file << "_";
                           out << std::setfill('0') << std::setw(6) << frame_count << " ";
                           out << static_cast<int>(d[1]) << " ";
                           out << score << " ";
                           out << static_cast<int>(d[3] * img.cols) << " ";
                           out << static_cast<int>(d[4] * img.rows) << " ";
                           out << static_cast<int>(d[5] * img.cols) << " ";
                           out << static_cast<int>(d[6] * img.rows) << std::endl;
                         */

                        cv::Point pt1, pt2;
                        pt1.x = (img.cols*d[3]);
                        pt1.y = (img.rows*d[4]);
                        pt2.x = (img.cols*d[5]);
                        pt2.y = (img.rows*d[6]);
                        int index = static_cast<int>(d[1]);
                        int green = 255 * ((index + 1) % 3);
                        int blue = 255 * (index % 3);
                        int red = 255 * ((index + 1) % 2);
                        cv::rectangle(img, pt1, pt2, cvScalar(red, green, blue), 1, 8, 0);

                        char label[100];
                        sprintf(label, "%s,%f", CLASSES[static_cast<int>(d[1])], score);
                        int baseline;
                        cv::Size size = cv::getTextSize(label, cv::FONT_HERSHEY_SIMPLEX, 0.5, 0, &baseline);
                        cv::Point pt3;
                        pt3.x = pt1.x + size.width;
                        pt3.y = pt1.y - size.height;

                        cv::rectangle(img, pt1, pt3, cvScalar(red, green, blue), -1);

                        cv::putText(img, label, pt1, cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 0));


                    }
                }
                cv::imshow("show", img);

                if (count <= max)
                {
                    cv::Size size;
                    size.width = img.cols;
                    size.height = img.rows;
                    static cv::VideoWriter writer;    // cv::VideoWriter output_video;
                    if (count == 0)
                    {
                        writer.open("VideoTest.avi", CV_FOURCC('D', 'I', 'V', 'X'), 30, size);
                    }
                    else if (count == max)
                    {
                        writer << img;
                        writer.release();
                    }
                    else {
                        writer << img;
                    }


                    count++;

                }
                cv::waitKey(1);
                ++frame_count;
            }
            if (cap.isOpened())
            {
                cap.release();
            }
        }
    }
    return 0;
}

