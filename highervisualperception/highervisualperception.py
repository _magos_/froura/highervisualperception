#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time

from flask import Flask, request
from flask_restful import Resource, Api
import numpy as np
import os
from python import platerecognition
from python import vehicledetection
import json
import base64
app = Flask(__name__)
api = Api(app)

vehicle_datasource_dir = os.environ['IMAGE_DIR']
plates_list_csv = os.environ['EXTRACTED_INFORMATION_CSV_FILE']
os.system('rm ' + vehicle_datasource_dir + '/*')

def recognize_plate(vehicle_id):
    plate = platerecognition.recognize_plate(vehicle_datasource_dir + vehicle_id + '.jpg')
    return plate

def vehicle_detection(img_dir):
    return vehicledetection.detect_vehicle(img_dir)

def vehicle_data_handle(vehicle):
    with open(plates_list_csv, 'a') as f:
        """Get image binary"""
        img_bin = base64.decodebytes(bytes(vehicle, 'utf-8'))

        """Save img to server"""
        global img_counter
        img_dir = vehicle_datasource_dir +'/img_' + str(img_counter) + '.jpg'
        with open(img_dir, 'wb') as img_f:
            img_f.write(img_bin)

        # Vehicle detection.
        v_results = vehicle_detection(img_dir)
        # Plate recognition.
        v_plate = platerecognition.recognize_plate(img_dir)

        v_data = ''
        for res in v_results:
            if len(res) == 0:
                v_data = v_data + 'none,0,0:0:0:0:' + ':' + str(v_plate)
                continue
            v_name, v_score, v_x1, v_y1, v_x2, v_y2 = res

            v_data = v_name + ',' + v_score[:4] + ',' + v_x1 + ':' + v_y1 + ':' + v_x2 + ':' + v_y2 + ':' + str(v_plate) + '#' + v_data
        csv_data = str('\nimg_' + str(img_counter) + '.jpg' + ',' + str(v_data) + "," + str(v_plate))
        f.write(csv_data)
        img_counter += 1

        return v_data.replace(',', ':')[:-1]

img_counter = 0;
class Server(Resource):
    def post(self):
        req = request.data.decode('utf-8')
        a = json.loads(req.replace("\n","\\n"))
        vehicle = a['vehicles'].replace("\\n","\n")

        v_data = vehicle_data_handle(vehicle)

        return v_data


api.add_resource(Server, '/')

def main():
    app.run(host = '0.0.0.0', port = 3389, threaded = True, debug = True)

if __name__ == '__main__':
    main()

