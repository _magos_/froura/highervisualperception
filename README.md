# Higher Visual Perception

Frourá project is a smart camera system designed for intelligent transportation system (ITS).
Higher Visual Perception is the name of this program.
This program is responible for higher visual perception (vehicle classification, plate recognition, ...) of camera devices in the system.

# Overall

This program will be run on camera devices or server.
It will do some high perception with information which is sent by [Visual Perception](https://gitlab.com/_magos_/froura/visualperception.git), expected higher perception includes:

- [vehicle classification](https://github.com/eric612/MobileNet-YOLO.git).
- plate recognition.

Then, Higher Visual Perception will send percepted information to Visual Perception for further processing.

